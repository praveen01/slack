import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
//import Login from "./logIn";

import "antd/dist/antd.css";
import "./App.css";
import { Provider } from "react-redux";
// @ts-ignore
import configureServiceStore from "mattermost-redux/store";

// @ts-ignore
import { Client4 } from "mattermost-redux/client";

const offlineOptions = {
  persistOptions: {
    autoRehydrate: {
      log: false
    }
  }
};

const store = configureServiceStore({}, {}, offlineOptions);
Client4.setUrl("https://communication.hotelsoft.tech");

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
