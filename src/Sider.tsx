import React from "react";
import { Layout, Menu, Icon } from "antd";
import data from "./data.json";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
//@ts-ignore
import { createChannel } from "mattermost-redux/actions/channels";
//@ts-ignore
import { getChannels } from "mattermost-redux/actions/channels";
//@ts-ignore
import { getProfilesByIds } from "mattermost-redux/actions/users";
import { async } from "q";
const { Sider } = Layout;
type ChannelType = "O" | "P" | "D" | "G";
interface IChannel {
  id: string;
  create_at: number;
  update_at: number;
  delete_at: number;
  team_id: string;
  type: ChannelType;
  display_name: string;
  name: string;
  header: string;
  purpose: string;
  last_post_at: number;
  total_msg_count: number;
  extra_update_at: number;
  creator_id: string;
  scheme_id: string;
  isCurrent?: boolean;
  teammate_id?: string;
  status?: string;
  fake?: boolean;
  group_constrained: boolean;
}

interface ISiderState {
  [key: string]: IChannel[] | String | boolean;
  userName: String;
  displayName: String;
}

interface ISiderProps {
  users: any;
  allData: any;
  teamDetails: any;
  channelList: any;
}

class SiderBar extends React.Component<any, any> {
  handleClick(channelName: string) {}

  handleNewChannelOnClick = async () => {
    // <PopUp />;
  };

  handleChannelClick = (id: string, channelName: string) => {
    this.props.getChannelId(id, channelName);
  };

  shouldComponentUpdate(prevProps: any, prevState: any) {
    if (prevProps !== this.props) {
      return true;
    } else return false;
  }

  render() {
    return (
      <Sider style={{ height: "100vh", background: "grey", color: "blue" }}>
        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={["10"]}
          style={{ background: "grey", color: "blue" }}
        >
          <Menu.Item key="1" style={{ color: "White" }}>
            <span className="nav-text" style={{ textAlign: "left" }}>
              {this.props.userName !== " " ? (
                <div>
                  <p>
                    {this.props.displayName}

                    {`@${this.props.userName}`}
                  </p>
                </div>
              ) : (
                <h1></h1>
              )}
            </span>
          </Menu.Item>
          <Menu.Item key="2" style={{ color: "White", paddingTop: "5px" }}>
            <button
              style={{
                background: "green",
                border: "1px solid green",
                textAlign: "center"
              }}
              onClick={this.handleNewChannelOnClick}
            >
              New Channel +
            </button>
          </Menu.Item>
          {Object.keys(this.props.channelList).length > 0 ? (
            Object.keys(this.props.channelList).map((channelId: any) => {
              return (
                <Menu.Item
                  key={this.props.channelList[channelId].id}
                  style={{ color: "White", paddingTop: "5px" }}
                >
                  <span
                    className="nav-text"
                    style={{ textAlign: "left" }}
                    onClick={() =>
                      this.handleChannelClick(
                        channelId,
                        this.props.channelList[channelId].name
                      )
                    }
                  >
                    #{this.props.channelList[channelId].name}
                  </span>
                </Menu.Item>
              );
            })
          ) : (
            <p></p>
          )}
        </Menu>
      </Sider>
    );
  }
}
const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      createChannel,
      getChannels,
      getProfilesByIds
    },
    dispatch
  );

const mapStateToProps = (state: any) => {
  return {
    // entity: state.entities
    users: state.entities.users,
    allData: state,
    teamDetails: state.entities.teams.teams,
    channelList: state.entities.channels.channels
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SiderBar);

class PopUp extends React.Component {
  render() {
    return <></>;
  }
}
