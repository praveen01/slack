// import React, { Component } from "react";
// import { connect } from "react-redux";
// import { bindActionCreators } from "redux";
// // @ts-ignore
// import { login } from "mattermost-redux/actions/users";

// interface LoginProps {
//   login: (username: string, password: string) => any;
//   users: any;
// }

// class Login extends Component<LoginProps, {}> {
//   state = {
//     username: "",
//     password: ""
//   };
//   componentDidMount() {}

//   handleOnUsername = (e: any) => {
//     this.setState({
//       username: e.target.value
//     });
//   };

//   handleOnPassword = (e: any) => {
//     this.setState({
//       password: e.target.value
//     });
//   };

//   login = async () => {
//     // logic
//     // alert(this.state.username);
//     // alert(this.state.password);
//     const user = await this.props.login(
//       this.state.username,
//       this.state.password
//     );
//     console.log("after login ", user);
//     alert(user.data || user.error);
//   };

//   render() {
//     console.log("User from props: ", this.props.users);
//     return (
//       <div>
//         <input
//           type="text"
//           placeholder="username"
//           onChange={this.handleOnUsername}
//         />
//         <input
//           type="password"
//           placeholder="password"
//           onChange={this.handleOnPassword}
//         />

//         <button onClick={this.login}> Login </button>
//       </div>
//     );
//   }
// }

// const mapStateToProps = (state: any) => {
//   console.log("State: IN map state to props ", state);
//   return {
//     // entity: state.entities
//     users: state.entities.users
//   };
// };

// const mapDispatchToProps = (dispatch: any) =>
//   bindActionCreators(
//     {
//       login
//     },
//     dispatch
//   );

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(Login);

import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
//@ts-ignore
import { login } from "mattermost-redux/actions/users";

interface ILogInProps {
  login: (username: string, password: string) => any;
  users: any;
}
interface ILogInState {
  name: string;
  password: string;
  errorStatus: string;
}

class LogInForm extends React.Component<ILogInProps, ILogInState> {
  constructor(props: any) {
    super(props);
    this.state = {
      name: "",
      password: "",
      errorStatus: " "
    };
  }

  handleOnChangeName = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      name: e.target.value
    });
  };

  handleOnChangePassword = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      password: e.target.value
    });
  };

  handleOnClick = async (event: any) => {
    event.preventDefault();
    const user = await this.props.login(this.state.name, this.state.password);
    if (user.data) {
      window.location.assign("/chatPage");
    } else {
      this.setState({
        errorStatus: "Invalid Email or Password"
      });
    }
  };

  render() {
    console.log("in render in login");
    return (
      <div id="container">
        <form>
          {this.state.errorStatus !== " " ? (
            <div>{this.state.errorStatus}</div>
          ) : (
            <h1></h1>
          )}
          <div className="inputBox">
            <input
              type="text"
              placeholder="User Name"
              style={{
                color: "dark green",
                borderRadius: "5px"
              }}
              value={this.state.name}
              onChange={this.handleOnChangeName}
              required
            />
          </div>
          <div className="inputBox">
            <input
              style={{
                color: "dark green",
                borderRadius: "5px"
              }}
              type="password"
              placeholder="Password"
              value={this.state.password}
              onChange={this.handleOnChangePassword}
              required
            />
          </div>
          <div>
            <input type="checkbox" />
            Remember me
          </div>
          <div style={{ display: "inline" }}>
            <a style={{ float: "right" }}>Forgot Password</a>
          </div>
          <div>
            <button
              style={{
                background: "rgb(3, 148, 252)",
                borderRadius: "3px",
                width: "100%"
              }}
              onClick={e => this.handleOnClick(e)}
            >
              Log In
            </button>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    // entity: state.entities
    users: state.entities.users,
    teamDetails: state.entities.teams.teams
  };
};

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      login
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LogInForm);
