import React from "react";
import { Layout } from "antd";
import { Button } from "antd";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
//@ts-ignore
import { logout } from "mattermost-redux/actions/users";
const { Header } = Layout;

interface Iprops {
  channelName: string;
}
class HeaderBar extends React.Component<any, any> {
  handleOnClick = async () => {
    const data = await this.props.logout();
    if (data.data === true) {
      window.location.assign("/");
    } else {
      alert("some error");
    }
  };

  render() {
    console.log("in header", this.props);
    return (
      <Header
        style={{
          color: "white",
          textAlign: "left",
          position: "relative",
          background: "white"
        }}
      >
        <p style={{ color: "blue" }}>{this.props.channelName}</p>
        <Button
          type="danger"
          style={{
            float: "right",
            position: "absolute",
            bottom: "0",
            right: "0"
          }}
          onClick={this.handleOnClick}
        >
          Log Out
        </Button>
      </Header>
    );
  }
}
const mapStateToProps = (state: any) => {
  return {
    // entity: state.entities
    users: state.entities.users
  };
};

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      logout
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderBar);
