import React from "react";

import { Layout } from "antd";
import data from "./data.json";
import { useState } from "react";
//import Display from "./Display";
import Sider from "./Sider";
import Header from "./Header";
import Display from "./Content";
import Footer from "./Footer";
import Login from "./logIn";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
//@ts-ignore
import { getChannels } from "mattermost-redux/actions/channels";

//@ts-ignore
import { getPosts } from "mattermost-redux/actions/posts";

class chatPage extends React.Component<any, any> {
  userName = " ";
  displayName = " ";
  messageIds = " ";
  messageDetails = " ";
  channelList = " ";
  channelName = " ";
  constructor(props: any) {
    super(props);
    this.state = {
      channelList: "",
      userName: " ",
      displayName: " ",
      messageIds: " ",
      messageDetails: " ",
      channelName: " ",
      id: " "
    };
  }

  componentDidUpdate(prevprops: any, prevState: any) {
    if (prevprops.teamDetails !== this.props.teamDetails) {
      if (Object.keys(this.props.teamDetails).length > 0) {
        let teamId = Object.keys(this.props.teamDetails)[0];

        let displayName = this.props.teamDetails[teamId].display_name;
        this.displayName = displayName;
        let channelListData = Object.keys(this.props.channelList).map(key => {
          return this.props.channelList[key];
        });

        let getChannelList = async (teamId: string) => {
          let channelListData = await this.props.getChannels(teamId);

          this.setState(() => {
            return {
              channelList: channelListData.data,
              displayName: displayName,
              userName: this.userName,

              channelName: channelListData.data[0].name
            };
          });
        };
        if (Object.keys(this.props.channelList).length === 0) {
          getChannelList(teamId);
        } else {
          this.setState(() => {
            return {
              channelList: channelListData,
              displayName: displayName,
              userName: this.userName
            };
          });
        }
      }
      if (prevprops.users !== this.props.users) {
        let userId = this.props.users.currentUserId;
        let userName;
        if (this.props.users.currentUserId !== "") {
          userName = this.props.users.profiles[userId].username;
        }
        this.userName = userName;
      }
    }
  }

  getChannelId = (id: string, channelName: string) => {
    let getPostsOfChannel = async () => {
      let posts = await this.props.getPosts(id);
      this.setState(() => {
        return {
          messageIds: posts.data.order,
          messageDetails: posts.data.posts,
          channelName: channelName,
          id: id
        };
      });
    };
    getPostsOfChannel();
  };

  render() {
    console.log("in chat page all the details", this.props.allData);
    return (
      <div className="App">
        <Layout>
          <Sider
            getChannelId={this.getChannelId}
            channelList={this.state.channelList}
            userName={this.state.userName}
            displayName={this.state.displayName}
          />
          <Layout>
            <Header channelName={this.state.channelName} />

            <Display
              id={this.state.id}
              messageIds={this.state.messageIds}
              messageDetails={this.state.messageDetails}
              currentUserId={this.props.users.currentUserId}
              currentUserName={this.state.userName}
              allData={this.props.allData}
            />
            <Footer channelName={" "} />
          </Layout>
        </Layout>
      </div>
    );
  }
}
const mapStateToProps = (state: any) => {
  return {
    // entity: state.entities
    users: state.entities.users,
    allData: state,
    teamDetails: state.entities.teams.teams,
    channelList: state.entities.channels.channels
  };
};

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      getChannels,
      getPosts
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(chatPage);
