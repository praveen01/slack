import React from "react";
import "./App.css";
import { Layout, Menu, Icon } from "antd";
import data from "./data.json";
// import { useState } from "react";
//import Display from "./Display";
// import Sider from "./Sider";
// import Header from "./Header";
// import Display from "./Content";
// import Footer from "./Footer";
import Login from "./logIn";
import ChatPage from "./chatPage";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import chatPage from "./chatPage";

const App: React.FC = () => {
  // const [presentChannel, setPresentChannel] = useState(" ");

  // let setChannelName = (name: string) => {
  //   setPresentChannel(name);
  // };

  return (
    <Router>
      <Switch>
        <Route path="/" exact component={Login} />
        <Route path="/chatPage" component={chatPage} />
      </Switch>
    </Router>
  );
};

export default App;
