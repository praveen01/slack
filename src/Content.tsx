import React from "react";
import { Layout } from "antd";
import { bindActionCreators } from "redux";
import { Input } from "antd";
import { Card } from "antd";

import { connect } from "react-redux";
import chatPage from "./chatPage";
//@ts-ignore
import { createPost } from "mattermost-redux/actions/posts";
const { Content } = Layout;

interface IProps {
  id: String;
}

class Display extends React.Component<any, any> {
  state = {
    message: " "
  };
  handleOnChange = (event: any) => {
    this.setState({
      message: event.target.value
    });
  };

  handleKeyDown = async (event: any) => {
    if (event.key === "Enter") {
      console.log("entered");
      let result = await this.props.createPost({
        message: this.state.message,
        channel_id: this.props.id,
        user_id: this.props.currentUserId,
        props: { username: this.props.currentUserName }
      });
      this.setState({
        message: " "
      });
    }
  };

  render() {
    return (
      <Content style={{ alignItems: "left" }}>
        <div
          id="messages"
          style={{
            width: "auto",
            height: "90vh",
            overflow: "auto"
          }}
        >
          {this.props.messageDetails !== " " ? (
            this.props.messageIds.map((messageId: string) => {
              return (
                <div
                  key={messageId}
                  style={{ background: "#ECECEC", padding: "5px" }}
                >
                  <Card
                    title={this.props.messageDetails[messageId].props.username}
                    bordered={false}
                    style={{ width: "auto" }}
                  >
                    <p>{this.props.messageDetails[messageId].message}</p>
                  </Card>
                </div>
              );
            })
          ) : (
            <h1></h1>
          )}
        </div>
        {this.props.id !== " " ? (
          <div style={{ textAlign: "left" }}>
            <Input
              placeholder="Enter Your Message"
              style={{
                position: "absolute",
                bottom: "0vh",
                width: "100vh",
                border: "1px solid black"
              }}
              value={this.state.message}
              onChange={this.handleOnChange}
              onKeyDown={this.handleKeyDown}
            />
          </div>
        ) : (
          <h1></h1>
        )}
      </Content>
    );
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators(
    {
      createPost
    },
    dispatch
  );
};

export default connect(
  null,
  mapDispatchToProps
)(Display);
